initSubModule:
	git submodule add https://bitbucket.org/joice_fonsecaa/scripts
	git submodule foreach git pull origin master
hooks:
	GO111MODULE=on go get github.com/golangci/golangci-lint/cmd/golangci-lint
	ln -f .git/hooks/pre-commit scripts/pre-commit.sh